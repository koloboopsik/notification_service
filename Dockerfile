FROM python:3.11
RUN apt-get update -y
RUN apt-get upgrade -y

WORKDIR /app

COPY ./requirements.txt ./
RUN pip install -r requirements.txt
COPY ./ ./

#RUN [ "python3", "./notification_service/manage.py", "migrate"]
CMD [ "python3", "./notification_service/manage.py", "runserver", "0.0.0.0:8000"]
