from collections import OrderedDict

from django.test import TestCase

from client.models import Client
from client.serializers import ClientSerializer


class ClientSerializerTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        client_model_obj_1 = Client.objects.create(
            mobile_number='79999999999',
            mobile_operator_code='999',
            timezone='Europe/Moscow',
        )
        client_model_obj_2 = Client.objects.create(
            mobile_number='78889999999',
            mobile_operator_code='888',
            timezone='Europe/Moscow',
        )
        cls.valid_data_for_deserialization = {'mobile_number': '79999999999', 'mobile_operator_code': '999',
                                              'timezone': 'Europe/Moscow', 'tags': []}
        cls.expected_data = [OrderedDict([('id', client_model_obj_1.id), ('mobile_number', '79999999999'),
                                          ('mobile_operator_code', '999'),
                                          ('timezone', 'Europe/Moscow'), ('tags', [])]),
                             OrderedDict([('id', client_model_obj_2.id), ('mobile_number', '78889999999'),
                                          ('mobile_operator_code', '888'),
                                          ('timezone', 'Europe/Moscow'), ('tags', [])])]

    def test_valid_data(self):
        queryset = Client.objects.all()
        serializer = ClientSerializer(queryset, many=True)
        self.assertEqual(serializer.data, self.expected_data)

        serializer = ClientSerializer(data=self.valid_data_for_deserialization)
        self.assertTrue(serializer.is_valid())
