import logging

from rest_framework.test import APITestCase

from client.models import Client

logging.getLogger("django").setLevel("ERROR")


class ClientViewsAPITestCase(APITestCase):
    @classmethod
    def setUpTestData(cls):
        for _ in range(10):
            Client.objects.create(
                mobile_number='79999999999',
                mobile_operator_code='999',
                timezone='Europe/Moscow',
            )

    def setUp(self):
        pass

    def test_list_of_client_view_set(self):
        response = self.client.get('/api/v1/client/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.json()), 10)

    def test_retrieve_of_client_view_set(self):
        client = Client.objects.last()
        response = self.client.get(f'/api/v1/client/{client.id}/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(list(response.json().keys()),
                         ['id', 'mobile_number', 'mobile_operator_code', 'timezone', 'tags'])

        response = self.client.get('/api/v1/client/string-slug/')
        self.assertEqual(response.status_code, 404)

    def test_create_of_client_view_set(self):
        client__pre_count = Client.objects.count()
        response = self.client.post('/api/v1/client/',
                                    data={'mobile_number': '79999999999', 'mobile_operator_code': '999',
                                          'timezone': 'Europe/Moscow', 'tags': []})
        self.assertEqual(response.status_code, 201)
        client__post_count = Client.objects.count()
        self.assertEqual(client__pre_count + 1, client__post_count)

        response = self.client.post('/api/v1/client/')
        self.assertEqual(response.status_code, 400)

    def test_update_of_client_view_set(self):
        client = Client.objects.last()
        response = self.client.put(f'/api/v1/client/{client.id}/',
                                   data={'mobile_number': '79999999998', 'mobile_operator_code': '999',
                                         'timezone': 'Europe/Moscow', 'tags': []})
        self.assertEqual(response.status_code, 200)
        client.refresh_from_db()
        self.assertEqual(client.mobile_number, '79999999998')

        response = self.client.put(f'/api/v1/client/{client.id}/')
        self.assertEqual(response.status_code, 400)

    def test_partial_update_of_client_view_set(self):
        client = Client.objects.last()
        response = self.client.patch(f'/api/v1/client/{client.id}/',
                                     data={'mobile_number': '79999999997'})
        self.assertEqual(response.status_code, 200)
        client.refresh_from_db()
        self.assertEqual(client.mobile_number, '79999999997')

    def test_destroy_of_client_view_set(self):
        client = Client.objects.last()
        response = self.client.delete(f'/api/v1/client/{client.id}/')
        self.assertEqual(response.status_code, 204)
        with self.assertRaises(Client.DoesNotExist):
            does_not_exist_client = Client.objects.get(pk=client.id)
