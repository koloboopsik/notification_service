from django.core.exceptions import ValidationError
from django.test import TestCase

from client.models import Client


class ClientModelTestCase(TestCase):
    def test_mobile_number_validation(self):
        client_model_obj = Client(
            mobile_number='79999999999',
            mobile_operator_code='999',
            timezone='Europe/Moscow',
        )
        client_model_obj.full_clean()

    def test_mobile_number_validation_error(self):
        with self.assertRaises(ValidationError) as e:
            client_model_obj = Client(
                mobile_number='89999999999',
                mobile_operator_code='999',
                timezone='Europe/Moscow',
            )
            client_model_obj.full_clean()
        self.assertEqual(list(e.exception.error_dict.keys()), ['mobile_number'])
        self.assertEqual(e.exception.error_dict['mobile_number'][0].messages[0],
                         'Номер телефона должен быть формате 7XXXXXXXXXX (X - цифра от 0 до 9)')

    def test_mobile_operator_code_validation(self):
        client_model_obj = Client(
            mobile_number='79999999999',
            mobile_operator_code='999',
            timezone='Europe/Moscow',
        )
        client_model_obj.full_clean()

    def test_mobile_operator_code_validation_error(self):
        with self.assertRaises(ValidationError) as e:
            client_model_obj = Client(
                mobile_number='79999999999',
                mobile_operator_code='asd',
                timezone='Europe/Moscow',
            )
            client_model_obj.full_clean()
        self.assertEqual(list(e.exception.error_dict.keys()), ['mobile_operator_code'])
        self.assertEqual(e.exception.error_dict['mobile_operator_code'][0].messages[0],
                         'Код мобильного оператора должен состоять из цифр')

        with self.assertRaises(ValidationError) as e:
            client_model_obj = Client(
                mobile_number='79999999999',
                mobile_operator_code='9999',
                timezone='Europe/Moscow',
            )
            client_model_obj.full_clean()
        self.assertEqual(list(e.exception.error_dict.keys()), ['mobile_operator_code'])
        self.assertEqual(e.exception.error_dict['mobile_operator_code'][0].messages[0],
                         'Ensure this value has at most 3 characters (it has 4).')
