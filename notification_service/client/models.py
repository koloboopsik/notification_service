from django.core.validators import RegexValidator, int_list_validator
from django.db import models

from mailing.models import Tag


class Client(models.Model):
    mobile_number = models.CharField(
        'Номер телефона',
        max_length=11,
        validators=[
            RegexValidator(
                regex=r'^7\d{10}$',
                message='Номер телефона должен быть формате 7XXXXXXXXXX (X - цифра от 0 до 9)'
            ),
        ],
    )
    mobile_operator_code = models.CharField(
        'Код мобильного оператора',
        max_length=3,
        validators=[
            int_list_validator(
                sep='',
                message='Код мобильного оператора должен состоять из цифр',
            ),
        ],
    )
    timezone = models.CharField(
        'Часовой пояс',
        max_length=128,
    )

    tags = models.ManyToManyField(
        Tag,
        blank=True,
    )

    class Meta:
        verbose_name = 'Клиент'
        verbose_name_plural = 'Клиенты'
