from rest_framework import viewsets

from client import models
from client.serializers import ClientSerializer


class ClientViewSet(viewsets.ModelViewSet):
    queryset = models.Client.objects.all()
    serializer_class = ClientSerializer
