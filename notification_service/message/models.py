from django.db import models

from client.models import Client
from mailing.models import Mailing


class MessageStatus(models.Model):
    name = models.CharField(
        'Имя',
        max_length=128,
    )

    class Meta:
        verbose_name = 'Статус сообщения'
        verbose_name_plural = 'Статусы сообщений'

    def __str__(self):
        return self.name


class Message(models.Model):
    status = models.ForeignKey(
        MessageStatus,
        on_delete=models.CASCADE,
        verbose_name='Статус сообщения',
    )

    created = models.DateTimeField(
        'Время создания (отправки)',
        auto_now_add=True,
    )

    mailing = models.ForeignKey(
        Mailing,
        on_delete=models.CASCADE,
        verbose_name='Рассылка',
    )
    client = models.ForeignKey(
        Client,
        on_delete=models.CASCADE,
        verbose_name='Клиент',
    )

    class Meta:
        verbose_name = 'Сообщение'
        verbose_name_plural = 'Сообщения'
        unique_together = [['mailing', 'client']]
