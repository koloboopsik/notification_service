from rest_framework import serializers

from . import models


class MessageStatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.MessageStatus
        fields = '__all__'


class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Message
        fields = '__all__'


class DetailMailingStatisticSerializer(serializers.Serializer):
    status__name = serializers.CharField(
        label='Статус сообщения',
        max_length=128,
        read_only=True,
    )
    count = serializers.IntegerField(
        label='Количество сообщений',
        read_only=True,
    )


class GeneralMailingStatisticSerializer(serializers.Serializer):
    mailing_id = serializers.IntegerField(
        label='ID рассылки',
        read_only=True,
    )
    statistics = DetailMailingStatisticSerializer(
        label='Статистика рассылки',
        read_only=True,
        many=True,
    )
