from drf_yasg.utils import swagger_auto_schema
from rest_framework import views
from rest_framework.response import Response
from rest_framework.viewsets import ReadOnlyModelViewSet

from . import models
from .serializers import MessageSerializer, MessageStatusSerializer, DetailMailingStatisticSerializer, \
    GeneralMailingStatisticSerializer
from .services.statistics import get_general_statistics_on_mailings, get_detail_statistics_on_mailing


class MessageViewSet(ReadOnlyModelViewSet):
    queryset = models.Message.objects.all()
    serializer_class = MessageSerializer


class MessageStatusViewSet(ReadOnlyModelViewSet):
    queryset = models.MessageStatus.objects.all()
    serializer_class = MessageStatusSerializer


class GeneralMailingStatisticView(views.APIView):
    @swagger_auto_schema(responses={200: GeneralMailingStatisticSerializer(many=True)})
    def get(self, request):
        statistics = get_general_statistics_on_mailings()
        return Response(statistics)


class DetailMailingStatisticView(views.APIView):
    @swagger_auto_schema(responses={200: DetailMailingStatisticSerializer(many=True)})
    def get(self, request, mailing_id):
        statistics = get_detail_statistics_on_mailing(mailing_id)
        return Response(statistics)
