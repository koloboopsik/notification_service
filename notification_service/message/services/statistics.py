from collections import defaultdict

from django.db.models import Count

from message.models import Message


def _group_mailings_stat(messages_statistics) -> list:
    """
    Группировка статистики по рассылкам.
    :param messages_statistics: список словарей с ключами mailing_id status__name count.
    :return: список словарей в формате
    {
        'mailing_id': 1,
        'statistics':
        [
            {
                'status__name': 'Отправлено',
                'count': 1
            },
            ...
        ]
     }
    """
    # добавляем статистику к конкретной рассылке
    # сделано потому что если у рассылки есть сообщения с разным статусом,
    # они находятся в разных элементах списка messages_statistics
    # todo подумать как можно лучше сделать этот момент
    statistics_dict = defaultdict(list)
    for stats_item in messages_statistics:
        mailing_id = stats_item.pop("mailing_id")
        statistics_dict[mailing_id].append(stats_item)
    # формируем более читабельный список словарей
    statistics_list = []
    for mailing_id, statistics in statistics_dict.items():
        mailing_statistics = {
            "mailing_id": mailing_id,
            "statistics": statistics,
        }
        statistics_list.append(mailing_statistics)
    return statistics_list


def get_general_statistics_on_mailings() -> list:
    """
    Получить общую статистику по рассылкам.
    """
    # Получаем список словарей с ключами mailing_id status__name count.
    messages_statistics = (
        Message.objects
        .select_related('status', 'mailing')
        .values('status__name')
        .annotate(count=Count('status_id'))
        .values('status__name', 'count', 'mailing_id')
    )
    statistics = _group_mailings_stat(messages_statistics)
    return statistics


def get_detail_statistics_on_mailing(mailing_id):
    """
    :param mailing_id: id рассылки
    :return: статистика по конкретной рассылке
    """
    statistics = (
        Message.objects.
        filter(mailing_id=mailing_id)
        .select_related('status')
        .values('status__name')
        .annotate(count=Count('status_id'))
        .values('status__name', 'count')
    )
    return statistics
