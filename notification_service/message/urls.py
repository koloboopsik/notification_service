from django.urls import path
from rest_framework import routers

from . import views

router = routers.SimpleRouter()
router.register('message', views.MessageViewSet)
router.register('message-status', views.MessageStatusViewSet)

urlpatterns = [
    path('general-statistics/', views.GeneralMailingStatisticView.as_view(), name='general-statistics'),
    path('detail-statistics/<int:mailing_id>/', views.DetailMailingStatisticView.as_view(), name='detail-statistics'),
]
