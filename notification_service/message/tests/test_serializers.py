import zoneinfo
from collections import OrderedDict

from django.db.models.signals import post_save
from django.test import TestCase

from client.models import Client
from mailing.models import Mailing
from mailing.signals import on_post_save_mailing
from message.models import Message, MessageStatus
from message.serializers import MessageSerializer, MessageStatusSerializer


class MessageSerializerTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        # отключаем создание задачи рассылки
        post_save.disconnect(on_post_save_mailing, sender=Mailing)
        mailing = Mailing.objects.create(
            start_time='2023-01-13T14:12:08.144115+03:00',
            end_time='2023-01-13T15:12:08.144315+03:00',
            message_text='Test message',
        )
        status = MessageStatus.objects.create(
            name='Создано',
        )
        client_1 = Client.objects.create(
            mobile_number='79999999999',
            mobile_operator_code='999',
            timezone='Europe/Moscow',
        )
        client_2 = Client.objects.create(
            mobile_number='78889999999',
            mobile_operator_code='888',
            timezone='Europe/Moscow',
        )
        message_model_obj_1 = Message.objects.create(
            status=status,
            mailing=mailing,
            client=client_1,
        )
        message_model_obj_2 = Message.objects.create(
            status=status,
            mailing=mailing,
            client=client_2,
        )
        tz = zoneinfo.ZoneInfo('Europe/Moscow')
        cls.expected_data = [OrderedDict([('id', message_model_obj_1.id),
                                          ('created', message_model_obj_1.created.astimezone(tz).isoformat()),
                                          ('status', status.id), ('mailing', mailing.id), ('client', client_1.id)]),
                             OrderedDict([('id', message_model_obj_2.id),
                                          ('created', message_model_obj_2.created.astimezone(tz).isoformat()),
                                          ('status', status.id), ('mailing', mailing.id), ('client', client_2.id)])]

    def test_valid_data(self):
        queryset = Message.objects.all()
        serializer = MessageSerializer(queryset, many=True)
        self.assertEqual(serializer.data, self.expected_data)


class MessageStatusSerializerTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        message_status_1 = MessageStatus.objects.create(name='Отправлено')
        message_status_2 = MessageStatus.objects.create(name='Создано')
        cls.expected_data = [OrderedDict([('id', message_status_1.id), ('name', 'Отправлено')]),
                             OrderedDict([('id', message_status_2.id), ('name', 'Создано')])]

    def test_valid_data(self):
        queryset = MessageStatus.objects.all()
        serializer = MessageStatusSerializer(queryset, many=True)
        self.assertEqual(serializer.data, self.expected_data)
