from django.db import IntegrityError
from django.db.models.signals import post_save
from django.test import TestCase

from client.models import Client
from mailing.models import Mailing
from mailing.signals import on_post_save_mailing
from message.models import Message, MessageStatus


class MessageModelTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        # отключаем создание задачи рассылки
        post_save.disconnect(on_post_save_mailing, sender=Mailing)
        cls.mailing = Mailing.objects.create(
            start_time='2023-01-13T14:12:08.144115+03:00',
            end_time='2023-01-13T15:12:08.144315+03:00',
            message_text='Test message',
        )
        cls.status = MessageStatus.objects.create(
            name='Создано',
        )
        cls.client_model_obj = Client.objects.create(
            mobile_number='79999999999',
            mobile_operator_code='999',
            timezone='Europe/Moscow',
        )

    def test_unique_together_mailing_client(self):
        message_model_obj_1 = Message.objects.create(
            status=self.status,
            mailing=self.mailing,
            client=self.client_model_obj,
        )
        message_model_obj_1.full_clean()
        with self.assertRaises(IntegrityError):
            Message.objects.create(
                status=self.status,
                mailing=self.mailing,
                client=self.client_model_obj,
            )
