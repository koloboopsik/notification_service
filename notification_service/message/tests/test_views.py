import logging
from datetime import timedelta

from django.db.models.signals import post_save
from django.utils.timezone import localtime
from rest_framework.test import APITestCase

from client.models import Client
from mailing.models import Mailing
from mailing.signals import on_post_save_mailing
from message.models import Message, MessageStatus

logging.getLogger("django").setLevel("ERROR")


class MessageViewsAPITestCase(APITestCase):
    @classmethod
    def setUpTestData(cls):
        # отключаем создание задачи рассылки
        post_save.disconnect(on_post_save_mailing, sender=Mailing)

        cls.mailing = Mailing.objects.create(
            start_time=localtime(),
            end_time=localtime() + timedelta(hours=1),
            message_text='Test message',
        )
        status = MessageStatus.objects.create(
            name='Создано',
        )

        for _ in range(10):
            client = Client.objects.create(
                mobile_number='79999999999',
                mobile_operator_code='999',
                timezone='Europe/Moscow',
            )
            Message.objects.create(
                status=status,
                mailing=cls.mailing,
                client=client,
            )

    def test_list_of_message_view_set(self):
        response = self.client.get('/api/v1/message/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.json()), 10)

    def test_retrieve_of_message_view_set(self):
        message = Message.objects.last()
        response = self.client.get(f'/api/v1/message/{message.id}/')
        self.assertEqual(response.status_code, 200)

        response = self.client.get('/api/v1/message/string-slug/')
        self.assertEqual(response.status_code, 404)

    def test_list_of_message_status_view_set(self):
        response = self.client.get('/api/v1/message-status/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.json()), 1)

    def test_retrieve_of_message_status_view_set(self):
        message_status = MessageStatus.objects.last()
        response = self.client.get(f'/api/v1/message-status/{message_status.id}/')
        self.assertEqual(response.status_code, 200)

        response = self.client.get('/api/v1/message/string-slug/')
        self.assertEqual(response.status_code, 404)

    def test_general_mailing_statistic_view(self):
        response = self.client.get('/general-statistics/')
        self.assertEqual(response.status_code, 200)
        response_data = response.json()
        self.assertIsInstance(response_data, list)
        self.assertEqual(len(response_data), 1)
        self.assertIsInstance(response_data[0], dict)
        self.assertListEqual(list(response_data[0].keys()), ['mailing_id', 'statistics'])
        self.assertIsInstance(response_data[0]['statistics'], list)
        self.assertListEqual(list(response_data[0]['statistics'][0].keys()), ['status__name', 'count'])

    def test_detail_mailing_statistic_view(self):
        response = self.client.get(f'/detail-statistics/{self.mailing.id}/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), [{'status__name': 'Создано', 'count': 10}])
