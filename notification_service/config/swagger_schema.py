from drf_yasg import openapi
from drf_yasg.generators import OpenAPISchemaGenerator
from drf_yasg.views import get_schema_view as default_get_schema_view
from rest_framework.permissions import AllowAny


class _CustomSchemaGenerator(OpenAPISchemaGenerator):
    def get_schema(self, request=None, public=False):
        schema = super().get_schema(request, public)
        schema.schemes = [
            "http",
        ]
        return schema


def get_schema_view(title: str, version: str):
    return default_get_schema_view(
        openapi.Info(
            title=title,
            default_version=version,
            contact=openapi.Contact(
                name='Для контакта: Telegram',
                url='https://t.me/koloboopsik/',
            ),
        ),
        public=True,
        generator_class=_CustomSchemaGenerator,
        permission_classes=[AllowAny],
    )
