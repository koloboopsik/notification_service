"""notification_service URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from rest_framework import routers

from client.urls import router as client_router
from config.swagger_schema import get_schema_view
from mailing.urls import router as mailing_router
from message.urls import router as message_router

router = routers.DefaultRouter()
router.registry.extend(client_router.registry)
router.registry.extend(mailing_router.registry)
router.registry.extend(message_router.registry)

swagger_schema_view = get_schema_view(title="Automailing", version="1.0")

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/v1/', include(router.urls)),
    path('', include('message.urls')),
    path("docs/", swagger_schema_view.with_ui("swagger", cache_timeout=0), name="schema-swagger-ui"),
]
