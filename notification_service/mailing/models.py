from django.core.validators import int_list_validator
from django.db import models


class Tag(models.Model):
    name = models.CharField(
        'Имя',
        max_length=128,
    )

    class Meta:
        verbose_name = 'Тег'
        verbose_name_plural = 'Теги'

    def __str__(self):
        return self.name


class Mailing(models.Model):
    start_time = models.DateTimeField('Время начала рассылки')
    end_time = models.DateTimeField('Время окончания рассылки')
    message_text = models.TextField('Текст рассылки')

    mobile_operator_code = models.CharField(
        'Код мобильного оператора',
        max_length=3,
        blank=True,
        null=True,
        validators=[
            int_list_validator(
                sep='',
                message='Код мобильного оператора должен состоять из цифр',
            ),
        ],
    )

    tags = models.ManyToManyField(
        Tag,
        blank=True,
    )

    class Meta:
        verbose_name = 'Рассылка'
        verbose_name_plural = 'Рассылки'


class MailingTask(models.Model):
    created_at = models.DateTimeField(
        'Время создания',
        auto_now_add=True,
    )
    task_id = models.CharField(
        'ID задачи в celery',
        max_length=36,
    )
    CREATED = 'created'
    DONE = 'done'
    RETRY = 'retry'
    ETA_CHANGED = 'eta_changed'
    STATUSES = (
        (CREATED, 'Создана'),
        (DONE, 'Завершена'),
        (RETRY, 'Повторная попытка'),
        (ETA_CHANGED, 'Время начала задачи изменено'),
    )
    status = models.CharField(
        'Статус',
        max_length=50,
        choices=STATUSES,
        default=CREATED,
    )

    mailing = models.OneToOneField(
        Mailing,
        on_delete=models.CASCADE,
        verbose_name='Рассылка',
    )

    class Meta:
        verbose_name = 'Задача рассылки'
        verbose_name_plural = 'Задачи рассылок'

    def __str__(self):
        return f'Задача рассылки {self.mailing_id} {self.get_status_display()}'
