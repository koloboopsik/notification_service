import json
import logging
import traceback
from typing import Union

import requests
from django.conf import settings

celery_worker_logger = logging.getLogger(f'celery_worker.{__name__}')


class BaseMessageSender:
    def __init__(self, *args, **kwargs):
        pass

    def send_message(self, *args, **kwargs):
        """
        Отправка одного сообщения из рассылки.
        :return: Флаг состояния, сообщение
        """
        raise NotImplementedError(f'Переопределите метод {self.send_message.__name__}')


class APIMessageSender(BaseMessageSender):
    """
    Класс для отправки сообщений на апи probe.fbrq.cloud
    """
    URL = "https://probe.fbrq.cloud/v1/send/{msg_id}"

    def get_url(self, msg_id: int) -> str:
        """
        Получение url для запроса.
        :param msg_id: id сообщения
        :return: url
        """
        return self.URL.format(msg_id=msg_id)

    @staticmethod
    def get_headers() -> dict:
        """
        Получение словаря заголовков.
        """
        headers = {
            "accept": "application/json",
            "Authorization": f"Bearer {settings.API_TOKEN}",
        }
        return headers

    @staticmethod
    def get_payload(msg_id: int, phone: Union[int, str], text: str) -> str:
        """
        Получение тела запроса в формате строки json.
        :param msg_id: id сообщения
        :param phone: номер телефона
        :param text: текст сообщения
        :return: строка с json словарём
        """
        payload = {
            "id": msg_id,
            "phone": int(phone),
            "text": text,
        }
        payload = json.dumps(payload)
        return payload

    def send_message(self, msg_id: int, phone: Union[int, str], text: str, **kwargs) -> tuple[bool, str]:
        """
        Отправка запроса (сообщения) на апи probe.fbrq.cloud
        :param msg_id: id сообщения
        :param phone: номер телефона
        :param text: текст сообщения
        :return: флаг состояния, сообщение
        """
        url = self.get_url(msg_id)
        headers = self.get_headers()
        payload = self.get_payload(msg_id, phone, text)
        celery_worker_logger.debug(f'Отправка запроса на api {url} сообщения {msg_id=}')
        try:
            response = requests.post(url, headers=headers, data=payload)
        except Exception as e:
            celery_worker_logger.debug(f'Ошибка при отправке запроса.\n{traceback.format_exc()}')
            return False, str(e)

        if response.status_code == 200:
            celery_worker_logger.debug(f'Запрос для {msg_id=} успешно отправлен. Пришел ответ: {response.content}')
            return True, str(response.status_code)

        celery_worker_logger.debug(f'Ошибка {response.status_code} при запросе для {msg_id=}. '
                                   f'Пришел ответ: {response.content}')
        return False, str(response.status_code)


if __name__ == '__main__':
    import os

    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'config.settings')
    obj = APIMessageSender()
    obj.send_message(1, 1, '1')
