import logging

from celery.result import AsyncResult
from django.utils.timezone import localtime

from config import celery_app
from mailing.models import Mailing, MailingTask
from mailing.tasks import send_mailing_task

notification_service_logger = logging.getLogger(f'notification_service.{__name__}')


class MailingTaskStarter:
    """
    Класс отправляет в очередь задачи рассылок.
    Создаёт новые задачи, и может поменять время уже существующей задачи.
    """

    def __init__(self, mailing: Mailing, created: bool):
        self.mailing = mailing
        self.created = created

    def start_task(self):
        if self.created:
            self.start_new_mailing_task()
        else:
            self.restart_mailing_task_on_changed_start_time()

    def start_new_mailing_task(self) -> None:
        """
        Запуск новой задачи рассылки.
        """
        now_time = localtime()
        if self.mailing.start_time < now_time < self.mailing.end_time:
            task = self._start_new_mailing_task_immediately()
            self._create_mailing_task_obj(task)
        elif now_time < self.mailing.start_time < self.mailing.end_time:
            task = self._start_new_mailing_task_with_delay()
            self._create_mailing_task_obj(task)
        else:
            notification_service_logger.warning(f'Странное время рассылки mailing_id={self.mailing.id} '
                                                f'[{self.mailing.start_time}] - [{self.mailing.end_time}] '
                                                f'Рассылка не запущена.')

    def restart_mailing_task_on_changed_start_time(self) -> None:
        """
        Перезапуск почтовой задачи при изменении времени начала.
        Перезапускаем задачу, если поменялось время старта и время конца не истекло.
        """
        if self._restart_condition():
            notification_service_logger.info(f'У рассылки mailing_id={self.mailing.id} поменяли время начала')
            mailingtask = MailingTask.objects.get(mailing_id=self.mailing.id)

            if mailingtask.status == MailingTask.DONE:
                notification_service_logger.warning(f'Рассылка mailing_id={self.mailing.id} уже завершена. '
                                                    f'Рассылка не запущена.')
                return

            # убираем старую задачу рассылки из очереди
            self._revoke_mailing_task(mailingtask.task_id)
            # и создаем новую задачу
            task = self._start_new_mailing_task_with_delay()

            mailingtask.task_id = task.id
            mailingtask.status = MailingTask.ETA_CHANGED
            mailingtask.save()

    def _start_new_mailing_task_immediately(self) -> AsyncResult:
        """
        Запуск рассылки немедленно.
        :return: Celery задача.
        """
        notification_service_logger.info(f'Запуск рассылки mailing_id={self.mailing.id} немедленно')

        task = send_mailing_task.apply_async(kwargs={"mailing_id": self.mailing.id})

        return task

    def _start_new_mailing_task_with_delay(self) -> AsyncResult:
        """
        Запуск рассылки с задержкой.
        Начало задачи совпадает со временем начала рассылки.
        :return: Celery задача.
        """
        notification_service_logger.info(f'Запуск рассылки mailing_id={self.mailing.id} в {self.mailing.start_time}')

        task = send_mailing_task.apply_async(
            kwargs={"mailing_id": self.mailing.id},
            eta=self.mailing.start_time,
        )

        return task

    def _create_mailing_task_obj(self, task: AsyncResult) -> None:
        """
        Создание объекта модели MailingTask.
        :param task: Celery задача.
        """
        MailingTask.objects.create(task_id=task.id, mailing_id=self.mailing.id, status=MailingTask.CREATED)

    def _restart_condition(self) -> bool:
        """
        Условия перезапуска задачи.
        :return: Флаг, перезапускать ли задачу.
        """
        # start_time изменился
        is_start_time_updated = (hasattr(self.mailing, 'cached_start_time') and
                                 not self.mailing.start_time == self.mailing.cached_start_time)
        # время end_time актуально
        is_end_time_actual = self.mailing.end_time > localtime()
        if not is_end_time_actual:
            notification_service_logger.warning(f'Время конца рассылки mailing_id={self.mailing.id} не актуально. '
                                                f'Задача не была запущена.')
        return is_start_time_updated and is_end_time_actual

    def _revoke_mailing_task(self, task_id: str) -> None:
        """
        Убираем старую задачу рассылки из очереди.
        :param task_id: id задачи.
        """
        notification_service_logger.info(f'Остановка рассылки mailing_id={self.mailing.id} {task_id=}')
        celery_app.control.revoke(task_id, terminate=True)
