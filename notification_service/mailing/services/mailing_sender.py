import logging
from typing import Union, Optional

from django.db.models import Q, Subquery, QuerySet
from django.utils.timezone import localtime

from client.models import Client
from mailing.models import Mailing
from mailing.services.api_message_sender import APIMessageSender
from message.models import Message, MessageStatus

celery_worker_logger = logging.getLogger(f'celery_worker.{__name__}')


class MailingSender:
    message_sender_class = APIMessageSender

    def __init__(self, mailing_id: int):
        self.mailing_id = mailing_id

        self._mailing: Optional[Mailing] = None
        self._created_message_status: Optional[MessageStatus] = None
        self._sent_message_status: Optional[MessageStatus] = None
        self._not_sent_message_status: Optional[MessageStatus] = None

    @property
    def mailing(self) -> Mailing:
        """
        Получение объекта Mailing (рассылки) из базы данных.
        """
        if self._mailing is None:
            self._mailing = Mailing.objects.prefetch_related('tags').get(pk=self.mailing_id)
        return self._mailing

    @property
    def is_mailing_end_time_actual(self) -> bool:
        """
        Время конца рассылки актуально.
        Возвращает True, если актуально, False если истекло.
        :return: Флаг, актуально ли время рассылки.
        """
        return self.mailing.end_time > localtime()

    @property
    def created_message_status(self) -> MessageStatus:
        """
        Получение статуса сообщения "Создано".
        """
        if self._created_message_status is None:
            self._created_message_status, _ = MessageStatus.objects.get_or_create(name='Создано')
        return self._created_message_status

    @property
    def sent_message_status(self) -> MessageStatus:
        """
        Получение статуса сообщения "Отправлено".
        """
        if self._sent_message_status is None:
            self._sent_message_status, _ = MessageStatus.objects.get_or_create(name='Отправлено')
        return self._sent_message_status

    @property
    def not_sent_message_status(self) -> MessageStatus:
        """
        Получение статуса сообщения "Не отправлено".
        """
        if self._not_sent_message_status is None:
            self._not_sent_message_status, _ = MessageStatus.objects.get_or_create(name='Не отправлено')
        return self._not_sent_message_status

    def get_message_sender(self, *args, **kwargs):
        message_sender_class = self.message_sender_class
        return message_sender_class(*args, **kwargs)

    def _filter_client_by_mobile_operator_code(self) -> Q:
        """
        Фильтр по коду мобильного оператора.
        """
        mobile_operator_code_filter = Q()

        mobile_operator_code = self.mailing.mobile_operator_code
        if mobile_operator_code:
            mobile_operator_code_filter &= Q(mobile_operator_code=mobile_operator_code)

        return mobile_operator_code_filter

    def _filter_client_by_tags(self) -> Q:
        """
        Фильтр клиентов по тегу.
        """
        tags_filter = Q()

        mailing_tags = self.mailing.tags.all()
        if mailing_tags:
            tags_filter &= Q(tags__in=mailing_tags)

        return tags_filter

    def _filter_client_by_sent_messages(self) -> Q:
        """
        Фильтруем клиентов, которым уже отправили сообщения по текущей рассылке.
        """
        sent_messages = Message.objects.filter(
            mailing_id=self.mailing_id,
            status=self.sent_message_status,
        ).values('client_id')
        client_sent_messages_filter = ~Q(id__in=Subquery(sent_messages))

        return client_sent_messages_filter

    def _get_client_filters(self) -> Q:
        """
        Получение всех фильтров для клиента.
        """
        client_filters = Q()

        client_filters &= self._filter_client_by_mobile_operator_code()
        client_filters &= self._filter_client_by_tags()
        client_filters &= self._filter_client_by_sent_messages()

        return client_filters

    def get_mobile_numbers_of_clients(self) -> QuerySet[list[tuple]]:
        """
        Получить мобильные номера всех клиентов подходящих под фильтр рассылки.
        :return: список словарей с id, мобильными номерами
        """
        client_filters = self._get_client_filters()
        recipients = Client.objects.filter(client_filters).values_list('id', 'mobile_number')

        return recipients

    def _create_message(self, client_id: int) -> Message:
        """
        Создание сообщения в бд.
        :param client_id: id клиента
        :return: объект Message
        """
        try:
            message = Message.objects.get(
                mailing=self.mailing,
                client_id=client_id,
            )
        except Message.DoesNotExist:
            message = Message.objects.create(
                status=self.created_message_status,
                mailing=self.mailing,
                client_id=client_id,
            )

        return message

    def _send_message(self, msg_id: int, phone: Union[int, str], text: str) -> tuple[bool, str]:
        message_sender = self.get_message_sender()
        success, msg = message_sender.send_message(msg_id, phone, text)
        return success, msg

    def send_mailing(self) -> bool:
        """
        Отправка рассылки для всех клиентов подходящих под фильтр рассылки.
        :return: статус, что все сообщения отправлены
        """
        all_messages_sent = True
        mobile_numbers = self.get_mobile_numbers_of_clients()
        celery_worker_logger.info(f'Начинается рассылка mailing_id={self.mailing_id} '
                                  f'для {len(mobile_numbers)} клиентов')

        for client_id, number in mobile_numbers:
            if not self.is_mailing_end_time_actual:
                celery_worker_logger.warning(f'Время конца рассылки mailing_id={self.mailing.id} истекло. '
                                             f'Завершаем рассылку.')
                # возвращаем True, чтобы рассылка не ушла в повтор
                return True

            message = self._create_message(client_id)
            success, response_msg = self._send_message(message.pk, number, self.mailing.message_text)
            if success:
                message.status = self.sent_message_status
                message.save()
                celery_worker_logger.info(f'Сообщение message_id={message.id} клиента {client_id=} '
                                          f'для рассылки mailing_id={self.mailing_id} успешно отправлено')
            else:
                all_messages_sent = False
                message.status = self.not_sent_message_status
                message.save()
                celery_worker_logger.error(f'Сообщение message_id={message.id} клиента {client_id=} '
                                           f'для рассылки mailing_id={self.mailing_id} не не отправлено. '
                                           f'Пришла ошибка: {response_msg}')

        return all_messages_sent
