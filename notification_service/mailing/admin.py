from django.contrib import admin

from mailing import models

admin.site.register(models.Tag)
admin.site.register(models.Mailing)
admin.site.register(models.MailingTask)
