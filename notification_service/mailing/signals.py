import logging

from django.db.models.signals import post_save, post_init
from django.dispatch import receiver

from mailing.services.mailing_task_starter import MailingTaskStarter

notification_service_logger = logging.getLogger(f'notification_service.{__name__}')


@receiver(signal=post_init, sender='mailing.Mailing')
def on_post_init_mailing(instance, **kwargs):
    """
    При инициализации модели Mailing.
    """
    # кешируем в объект модели поле start_time, для отслеживания его изменения
    instance.cached_start_time = instance.start_time


@receiver(post_save, sender='mailing.Mailing')
def on_post_save_mailing(sender, instance, created, **kwargs):
    """
    При изменении модели Mailing.
    """
    mailing_task_starter = MailingTaskStarter(instance, created)
    mailing_task_starter.start_task()
