from collections import OrderedDict

from django.db.models.signals import post_save
from django.test import TestCase

from mailing.models import Mailing, Tag
from mailing.serializers import MailingSerializer, TagSerializer
from mailing.signals import on_post_save_mailing


class MailingSerializerTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        # отключаем создание задачи рассылки
        post_save.disconnect(on_post_save_mailing, sender=Mailing)
        mailing_model_obj_1 = Mailing.objects.create(
            start_time='2023-01-13T14:12:08.144115+03:00',
            end_time='2023-01-13T15:12:08.144315+03:00',
            message_text='Test message',
        )
        mailing_model_obj_2 = Mailing.objects.create(
            start_time='2023-01-13T14:12:08.144115+03:00',
            end_time='2023-01-13T15:12:08.144315+03:00',
            message_text='Test message',
        )
        cls.valid_data_for_deserialization = {'start_time': '2023-01-13T14:12:08.144115+03:00',
                                              'end_time': '2023-01-13T15:12:08.144315+03:00',
                                              'message_text': 'Test message'}
        cls.invalid_data_end_time_for_deserialization = {'start_time': '2023-01-13T15:12:08.144315+03:00',
                                                         'end_time': '2023-01-13T14:12:08.144115+03:00',
                                                         'message_text': 'Test message'}
        cls.expected_data = [OrderedDict([('id', mailing_model_obj_1.id),
                                          ('start_time', '2023-01-13T14:12:08.144115+03:00'),
                                          ('end_time', '2023-01-13T15:12:08.144315+03:00'),
                                          ('message_text', 'Test message'), ('mobile_operator_code', None),
                                          ('tags', [])]),
                             OrderedDict([('id', mailing_model_obj_2.id),
                                          ('start_time', '2023-01-13T14:12:08.144115+03:00'),
                                          ('end_time', '2023-01-13T15:12:08.144315+03:00'),
                                          ('message_text', 'Test message'), ('mobile_operator_code', None),
                                          ('tags', [])])]

    def test_valid_data(self):
        queryset = Mailing.objects.all()
        serializer = MailingSerializer(queryset, many=True)
        self.assertEqual(serializer.data, self.expected_data)

        serializer = MailingSerializer(data=self.valid_data_for_deserialization)
        self.assertTrue(serializer.is_valid())

    def test_invalid_data(self):
        serializer = MailingSerializer(data=self.invalid_data_end_time_for_deserialization)
        self.assertFalse(serializer.is_valid())
        self.assertEqual(list(serializer.errors.keys()), ['end_time'])
        self.assertEqual(serializer.errors['end_time'][0], 'Время окончания должно быть больше, чем время начала')


class TagSerializerTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        tag_model_obj_1 = Tag.objects.create(name='Тег 1')
        tag_model_obj_2 = Tag.objects.create(name='Тег 2')
        cls.valid_data_for_deserialization = {"name": 'Тег 1'}
        cls.expected_data = [OrderedDict([('id', tag_model_obj_1.id), ('name', 'Тег 1')]),
                             OrderedDict([('id', tag_model_obj_2.id), ('name', 'Тег 2')])]

    def test_valid_data(self):
        queryset = Tag.objects.all()
        serializer = TagSerializer(queryset, many=True)
        self.assertEqual(serializer.data, self.expected_data)

        serializer = TagSerializer(data=self.valid_data_for_deserialization)
        self.assertTrue(serializer.is_valid())
