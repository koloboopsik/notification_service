import logging
from datetime import timedelta

from django.db.models.signals import post_save
from django.utils.timezone import localtime
from rest_framework.test import APITestCase

from mailing.models import Mailing
from mailing.signals import on_post_save_mailing

logging.getLogger("django").setLevel("ERROR")


class MailingViewsAPITestCase(APITestCase):
    @classmethod
    def setUpTestData(cls):
        # отключаем создание задачи рассылки
        post_save.disconnect(on_post_save_mailing, sender=Mailing)

        for _ in range(10):
            Mailing.objects.create(
                start_time=localtime(),
                end_time=localtime() + timedelta(hours=1),
                message_text='Test message',
            )

    def test_list_of_mailing_view_set(self):
        response = self.client.get('/api/v1/mailing/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.json()), 10)

    def test_retrieve_of_mailing_view_set(self):
        mailing = Mailing.objects.last()
        response = self.client.get(f'/api/v1/mailing/{mailing.id}/')
        self.assertEqual(response.status_code, 200)
        self.assertIsInstance(response.json(), dict)
        self.assertEqual(list(response.json().keys()),
                         ["id", "start_time", "end_time", "message_text", "mobile_operator_code", "tags", ])

        response = self.client.get('/api/v1/mailing/string-slug/')
        self.assertEqual(response.status_code, 404)

    def test_create_of_mailing_view_set(self):
        response = self.client.post('/api/v1/mailing/',
                                    data={'start_time': '2023-01-13T14:12:08.144115+03:00',
                                          'end_time': '2023-01-13T15:12:08.144315+03:00',
                                          'message_text': 'Test message'})
        self.assertEqual(response.status_code, 201)

        response = self.client.post('/api/v1/mailing/')
        self.assertEqual(response.status_code, 400)

    def test_update_of_mailing_view_set(self):
        mailing = Mailing.objects.last()
        response = self.client.put(f'/api/v1/mailing/{mailing.id}/',
                                   data={'start_time': '2023-01-13T14:12:08.144115+03:00',
                                         'end_time': '2023-01-13T15:12:08.144315+03:00',
                                         'message_text': 'Test message 123'})
        self.assertEqual(response.status_code, 200)
        mailing.refresh_from_db()
        self.assertEqual(mailing.message_text, 'Test message 123')

        response = self.client.put(f'/api/v1/mailing/{mailing.id}/')
        self.assertEqual(response.status_code, 400)

    def test_partial_update_of_mailing_view_set(self):
        mailing = Mailing.objects.last()
        response = self.client.patch(f'/api/v1/mailing/{mailing.id}/',
                                     data={'message_text': 'Test message 123456'})
        self.assertEqual(response.status_code, 200)
        mailing.refresh_from_db()
        self.assertEqual(mailing.message_text, 'Test message 123456')

    def test_destroy_of_mailing_view_set(self):
        mailing = Mailing.objects.last()
        response = self.client.delete(f'/api/v1/mailing/{mailing.id}/')
        self.assertEqual(response.status_code, 204)
        with self.assertRaises(Mailing.DoesNotExist):
            does_not_exist_client = Mailing.objects.get(pk=mailing.id)
