from django.core.exceptions import ValidationError
from django.test import TestCase

from mailing.models import Mailing


class MailingModelTestCase(TestCase):
    def test_mobile_operator_code_validation(self):
        mailing_model_obj = Mailing(
            start_time='2023-01-13T14:12:08.144115+03:00',
            end_time='2023-01-13T15:12:08.144315+03:00',
            message_text='Test message',
            mobile_operator_code='999',
        )
        mailing_model_obj.full_clean()

    def test_mobile_operator_code_validation_error(self):
        with self.assertRaises(ValidationError) as e:
            mailing_model_obj = Mailing(
                start_time='2023-01-13T14:12:08.144115+03:00',
                end_time='2023-01-13T15:12:08.144315+03:00',
                message_text='Test message',
                mobile_operator_code='aasd',
            )
            mailing_model_obj.full_clean()
        self.assertEqual(list(e.exception.error_dict.keys()), ['mobile_operator_code'])
        self.assertEqual(e.exception.error_dict['mobile_operator_code'][0].messages[0],
                         'Код мобильного оператора должен состоять из цифр')

        with self.assertRaises(ValidationError) as e:
            mailing_model_obj = Mailing(
                start_time='2023-01-13T14:12:08.144115+03:00',
                end_time='2023-01-13T15:12:08.144315+03:00',
                message_text='Test message',
                mobile_operator_code='9999',
            )
            mailing_model_obj.full_clean()
        self.assertEqual(list(e.exception.error_dict.keys()), ['mobile_operator_code'])
        self.assertEqual(e.exception.error_dict['mobile_operator_code'][0].messages[0],
                         'Ensure this value has at most 3 characters (it has 4).')
