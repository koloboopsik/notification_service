from rest_framework import serializers

from . import models


class TagSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Tag
        fields = '__all__'


class MailingSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Mailing
        fields = '__all__'

    def validate(self, data):
        if ('start_time' in data and 'end_time' in data and
                data['start_time'] > data['end_time']):
            raise serializers.ValidationError({"end_time": 'Время окончания должно быть больше, чем время начала'})
        return data
