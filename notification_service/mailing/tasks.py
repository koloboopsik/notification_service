import logging

from celery import shared_task
from django.conf import settings

from mailing.models import MailingTask
from mailing.services.mailing_sender import MailingSender

celery_worker_logger = logging.getLogger(f'celery_worker.{__name__}')


@shared_task(bind=True)
def send_mailing_task(self, mailing_id: int):
    celery_worker_logger.info(f'Начало задачи send_mailing_task {mailing_id=}')
    all_messages_sent = MailingSender(mailing_id).send_mailing()

    mailingtask = MailingTask.objects.get(mailing_id=mailing_id)

    # если не все сообщение отправлены перезапускаем задачу через settings.MAILING_RETRY_COUNTDOWN секунд
    if not all_messages_sent:
        celery_worker_logger.info(f'Повтор задачи send_mailing_task {mailing_id=} '
                                  f'через {settings.MAILING_RETRY_COUNTDOWN} секунд')
        self.retry(countdown=settings.MAILING_RETRY_COUNTDOWN, throw=False)

        mailingtask.status = MailingTask.RETRY
        mailingtask.save()

        return f'Рассылка отправлена на повтор {mailing_id=}'

    mailingtask.status = MailingTask.DONE
    mailingtask.save()

    celery_worker_logger.info(f'Конец задачи send_mailing_task {mailing_id=}')
    return f'Рассылка закончена {mailing_id=}'
