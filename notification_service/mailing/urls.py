from rest_framework import routers

from . import views

router = routers.SimpleRouter()
router.register('mailing', views.MailingViewSet)
router.register('tag', views.TagViewSet)
