# Сервис уведомлений - notification_service

## Запуск

1. Клонировать репозиторий


2. Создать файл .env в директории ./notification_service/config/

   _В итоге путь .env файла будет ./notification_service/config/.env_


3. В .env файле создать переменную API_TOKEN и поместить в него API ключ внешнего сервиса
   https://probe.fbrq.cloud/docs

   _API ключ должен быть без слова Bearer._

   _Шаблон .env файл находится на ./notification_service/config/.env.template_


4. Создать и запустить Docker контейнеры:

```
    docker-compose build
    docker-compose up
```

# Документация

Документация API эндпоинтов в формате OpenAPI находится на http://127.0.0.1:8000/docs/

Django-Админ панель находится на http://127.0.0.1:8000/admin/

Логи находятся в директории ./notification_service/logs/

Тесты можно запустить можно командой (перед этим нужно создать и запустить Docker контейнеры):

```
docker-compose run --rm notification_service sh -c "cd notification_service && python manage.py test"
```

# Как работает рассылка

После добавления в базу данных рассылки (POST http://127.0.0.1:8000/api/v1/mailing/)
создается celery задача c этой рассылкой. Задача запускается в сигнале post_save модели Mailing.
Если время начала рассылки меньше текущего она немедленно запускается, иначе создаётся задача с eta равному времени
начала рассылки.

Время используется по временной зоне - GMT+3 (Москва).

Время задачи меняется, при изменении времени рассылки (PUT http://127.0.0.1:8000/api/v1/mailing/<pk>/). Если во время
выполнения задачи рассылки, какие-либо сообщения
не дойдут до внешнего сервиса (https://probe.fbrq.cloud/v1/send/{msg_id}) задача будет повторена через
MAILING_RETRY_COUNTDOWN (находится в ./notification_service/config/settings) секунд
(уже дошедшие сообщения не будут отправляться).

